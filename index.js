console.log('Hello World');

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    const getProfile = ()=>{
        const fullName = prompt('What is your name?');
        const age = prompt('How old are you?');
        const address = prompt('Where do you live?');
        alert('Thank you for your input!');

        console.log(`Hello, ${fullName}`);
        console.log(`You are ${age} years old.`);
        console.log(`You live in ${address}`);
       
    }

    getProfile();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

    

	//second function here:
    const getFavBand = (favBandList)=>{
        favBandList.map((item, index)=> console.log(`${index + 1}. ${item}`));
    }

    const favBandList = ["The Beatles", "Metalica", "The Eagles", "L' arec~en~Ciel", "Eraserheads"];
    getFavBand(favBandList);

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    const getTopMovies = (movies)=>{
        movies.map( (item, index)=> console.log(`${index + 1}. ${item.title} \nRotten Tomatoes Rating: ${item.rating}%`))
    }

    const topMovies = [ 
        {title: 'The Godfather', rating: 97},
        {title: 'The Godfather, Part II', rating:96},
        {title: 'Shawshank Redemption', rating: 91},
        {title: 'To kill A Mockingbird', rating:93},
        {title: 'Psycho', rating: 96}
    ]

    getTopMovies(topMovies);

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);